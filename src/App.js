import React, { Component } from "react"
import {Header} from "components/Header"
import {Container} from "components/Container"
import {ArticlesList} from "components/ArticlesList"

class App extends Component {
  render() {
    return (
      <div className="app">
        <Header />
        <Container>
          <ArticlesList />
        </Container>
      </div>
    )
  }
}

export default App
