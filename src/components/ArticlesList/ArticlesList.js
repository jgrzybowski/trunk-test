import React, { PureComponent } from "react"
import {ArticleItem} from "./ArticleItem"
import {redditClient} from "services/reddit"
import {SORTING_MODES, nextSortingMode} from "./sorting-helpers"
import "./ArticlesList.scss"

class ArticlesList extends PureComponent {
  constructor(props) {
    super(props)

    this.state = {
      posts: [],
      sorting: SORTING_MODES[0].name
    }

    this.handleSortChange = this.handleSortChange.bind(this)
  }

  componentDidMount() {
    redditClient.getJSPosts().then((posts) => {
      this.setState({posts})
    })
  }

  handleSortChange() {
    this.setState(({sorting, posts}) => {
      const nextSorting = nextSortingMode(sorting)
      const nextPosts = posts.sort(nextSorting.compareFn)

      return {
        sorting: nextSorting.name,
        posts: nextPosts
      }
    })
  }

  render() {
    const {posts, sorting} = this.state

    return (
      <div className="articles-list">
        <div className="row align-right collapse">
          <div className="column shrink articles-sorting">
            <a onClick={this.handleSortChange} title="click to toggle">
              sorting by: {sorting}
            </a>
          </div>
        </div>

        {posts.map((post) => <ArticleItem {...post} />)}
      </div>
    )
  }
}

export {ArticlesList}
