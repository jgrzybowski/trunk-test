import React from "react"
import {LinkBtn} from "components/LinkBtn"
import {PropTypes} from "prop-types"

const ArticleItem = ({title, author, created, url}) => {
  const formattedDate = new Date(created * 1000).toLocaleString()

  return (
    <div className="article-item">
      <div className="row">
        <div className="column">
          <div className="article-title">{title}</div>
        </div>
      </div>

      <div className="row article-details">
        <div className="column">
          <span className="article-author">{author}</span><div className="article-date">{formattedDate}</div>
        </div>
        <div className="column shrink">
          <LinkBtn url={url} title="See article" />
        </div>
      </div>
    </div>
  )
}

ArticleItem.propTypes = {
  title: PropTypes.string.isRequired,
  author: PropTypes.string.isRequired,
  url: PropTypes.string.isRequired,
  created: PropTypes.number.isRequired
}

export {ArticleItem}
