const SORTING_MODES = [
  {
    name: "hottest",
    compareFn: (a, b) => {
      return a.key - b.key
    }
  },
  {
    name: "newest",
    compareFn: (a, b) => {
      return b.created - a.created
    }
  }
]

function nextSortingMode(currentModeName) {
  return SORTING_MODES.find((mode) => mode.name !== currentModeName)
}

export {SORTING_MODES, nextSortingMode}
