import React from "react"
import {PropTypes} from "prop-types"
import "./LinkBtn.scss"

const LinkBtn = ({title, url}) => {
  return (
    <a className="link-btn" target="_blank" href={url}>{title}</a>
  )
}

LinkBtn.propTypes = {
  title: PropTypes.string.isRequired,
  url: PropTypes.string.isRequired
}

export {LinkBtn}
