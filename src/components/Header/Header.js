import React from "react"
import "./Header.scss"

const Header = () => {
  return (
    <header className="app-header">
      <h1>Reddit JS</h1>
    </header>
  )
}

export {Header}
