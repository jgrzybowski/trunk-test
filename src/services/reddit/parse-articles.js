function parseArticles(payload) {
  return payload.data.children.map((item, key) => {
    const {author, title, url, created} = item.data

    return {author, title, url, created, key}
  })
}

export {parseArticles}
