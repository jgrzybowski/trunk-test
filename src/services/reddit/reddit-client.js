import axios from "axios"
import {parseArticles} from "./parse-articles"

const BASE_URI = "https://www.reddit.com"

class RedditClient {
  getJSPosts() {
    const path = "/r/javascript/top/.json"
    return this._get(path).then((resp) => parseArticles(resp.data))
  }

  _get(path) {
    const url = this._buildUrl(path)
    return axios.get(url)
  }

  _buildUrl(path) {
    return `${BASE_URI}${path}`
  }
}

const redditClient = new RedditClient()

export {redditClient}
